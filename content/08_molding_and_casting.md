+++
title = "Molding and Casting"
date = "2018-10-30"
menu = "main"
weight = 9
+++

Files: [gear.io](/designs/08_gear.io) (render with [libfive Studio](https://libfive.com/studio/))

This week we're machining 3d wax models on a desktop ShopBot, using them to make [silicone](https://www.smooth-on.com/products/oomoo-30/) molds, and using those to cast a variety of materials.


### Design

For a research project I've been interacting with a lot of functional representations of gears, so I thought it would be nice to make one physically. For research I've been using extruded gears like the one below, but that doesn't make a very exciting 3d model.

![](/img/08_involute_gear.jpg#c)

So I tapered the top, to produce a simple bevel gear. Now while it's true that the simple extruded gear is a proper involute gear (up to some imperfections introduced in the meshing process), the tapered version is not a proper involute bevel gear. But I won't be using this for mechanical purposes anyway ([Drystone](https://www.usg.com/content/usgcom/en/products/industrial/art-statuary/drystone-casting-media.html) is not an engineering material) so I can live with the imperfection. I also plan on machining deeper than the model into the wax, so that I'll essentially end up with a regular gear attached to a bevel gear.

![](/img/08_bevel_gear.jpg#c)

I sized the gear keeping in mind that the smallest slots between teeth would still have to admit an eighth inch end mill. I didn't bother filleting the sharp inside corners, since the [CAM software](https://www.vectric.com/products/vcarve.htm) I'll be using will implicitly add them to the toolpaths (as a by-product of only machining where the tool fits).


### Machining

I used an eighth inch flat end mill for both roughing and finishing. My finishing passes used a 10% stepover. I could get a smoother model with a ball end mill and smaller stepover, but this isn't rocket fabrication. Here's what my part looked like after roughing.

![](/img/08_post_roughing.jpg#c)

As you can see I made one mistake: I thought my toolpaths were relative to the corner of my stock, but instead they expected me to have set home to the top center. Hence the random cut through the side. It turns out this is because I clicked on the button for the corner in PartWorks 3d, which updates the UI, but neglected to hit apply, which makes it actually take effect for the toolpaths. I ended up filling the extra little hole with hot glue to ensure no uncured OOMOO could leak out.


### Mold Making

I could have machined a negative and cast drystone directly in the wax, but since my part has vertical walls the lack of a draft angle would make it difficult to remove the part in one piece. So making a flexible silicone mold is a good idea.

The [OOMOO](https://www.smooth-on.com/products/oomoo-30/) comes in two separate jars. Each contains a compound that is (decently) shelf stable on its own. Once you've mixed them, you have 15 minutes before they start solidifying. After mixing, I used a vacuum chamber to help remove bubbles that would otherwise compromise the mold.

The mold holds very fine detail. Though in my case, most of this detail is comprised of the cusps from my stepover.

![](/img/08_wax_and_oomoo.jpg#c)


### Casting

I decided to start with drystone. I found mixing it a little easier than the OOMOO, since it's not as goopy. The container indicated 100 parts drystone powder to 20 parts water, but I found that this produced a paste so thick I was worried about losing detail. So I added a bit more water until it had the consistency of a thick batter. I didn't do anything to clear bubbles, but this doesn't seem to have caused any issues.

![](/img/08_wax_oomoo_drystone.jpg#c)
![](/img/08_drystone_gear.jpg#c)

