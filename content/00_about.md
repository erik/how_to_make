+++
title = "About Me"
date = "2018-09-10"
menu = "main"
weight = 1
+++

Hi there! My name is Erik Strand.

I'm a student at [The Center for Bits and Atoms](http://cba.mit.edu). I like to use math and software to make better design and fabrication tools.

Previously, I studied math at the [University of Chicago](https://www.uchicago.edu/), consulted at [Keystone Strategy](https://www.keystonestrategy.com/), worked on the [FabLight Laser](https://www.3dfablight.com/) at [Otherlab](https://otherlab.com), and developed automation tools for CNC Milling at [Plethora](https://www.plethora.com).

This semester I'm taking MAS.863, also known as [How to Make (Almost) Anything](http://fab.cba.mit.edu/classes/863.18/). I will document all my projects for this course here.

