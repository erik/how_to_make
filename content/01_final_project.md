+++
title = "Final Project"
date = "2018-09-12"
menu = "main"
weight = 2
+++

This is my giant final project tracking page. It covers everything from abandoned ideas through implemented ones through my plans beyond this course.


## Idea 1: The Analog Analogue Synthesizer

Every electronic circuit has an analogous mechanical circuit whose motion is governed by an equivalent differential equation. Analog synthesizers are electronic circuits, so analog synthesizers have mechanical analogues -- analog analogue synthesizers.

The heart and soul of any classic subtractive synth is its filter section. Most use a ladder of RC circuits to make a low-pass filter (LPF). It takes a raw waveform with a lot of harsh high frequency content (like a sawtooth or triangle wave), and turns it into something more musical.

![](/img/01_rc_ladder.jpg#c)

Two rungs (pictured) give a two pole filter, and four, well, four. I'd like to make a mechanical analog of one such rung. If things go well, I can make multiple and chain them together, just like in the electronic circuit.


### Will it work?

In a normal analog synthesizer, voltage is used to represent air pressure (or more directly, the displacement of a speaker cone). In my analog analogue synthesizer, voltage will be replaced with force. The common electrical components then each have their own analog:

 - resistance <---> damping
 - capacitance <---> springiness
 - inductance <---> mass

So each rung of the ladder circuit will be, in my mechanical version, a damped oscillator.

To be more precise, a normal RC circuit is described by the following ODE:
$$rc \dot{y}(t) + y(t) = x(t)$$
Here \\(r\\) is the resistance in ohms, \\(c\\) the capacitance in farads, \\(x(t)\\) is the input voltage, and \\(y(t)\\) the output voltage (i.e. what we'll need to solve for).

A driven massless damped oscillator is described by this equation:
$$r \dot{y}(t) + k y(t) = x(t)$$
Here \\(r\\) is the damping coefficient (in Newtons per meters per second), \\(k\\) the spring constant (in Newtons per meter), \\(x(t)\\) the input force, and \\(y(t)\\) the displacement of the oscillator.

For appropriate values of the constants these two systems will clearly have the same behavior. So there's hope!

In order to keep forces and velocities within reasonable ranges, I'll aim to operate 100 times slower than normal audio. So a concert A, instead of being 440 Hz, will be just 4.4.


### How would it be made?

Since we'll represent the waveform-to-filter using force, the input has to be a force control servo. I know this can be done either via current measurement, or with a load cell looped into the controller. How hard could it be?

The damping could be provided with a viscous fluid. But, I don't know of viscous fluid dampers that can adjust their viscosity on the fly, which we'll need to do if we want to sweep the cutoff frequency of our filter. Which obviously we do. I only know of one other way to get a resistance force directly proportional to velocity: eddy braking. So we'll need an electromagnet (vary the current to vary the resistance) and an aluminum rail. What could go wrong?

The capacitor, luckily, just becomes a spring. Phew.

Now a normal RC filter contains no inductor. It would be difficult to make my mechanical filter's moving parts massless<sup>[citation needed]</sup>, but luckily this isn't strictly necessary. All RC circuits contain some self-inductance, as all mechanical circuits contain some mass. I'll just try to keep my filter's mass to a minimum relative to the damping and spring factors.

Finally, instead of measuring the output voltage across the capacitor, we'll be measuring the output force across the spring. So we need a load cell.

All in all, here's a simple sketch of the main components for one stage of the filter. If all goes well I'll build multiple of these.

![](/img/01_mechanical_design.jpg#c)



### Frequency Response Analysis

Since our mechanical oscillator won't be massless, we might as well model it with some mass. At least then we know what to expect. This gives us a second order ODE:
$$m \ddot{y}(t) + r \dot{y}(t) + k y(t) = x(t)$$
(The symbols are defined as above, except for the addition of the mass \\(m\\).)

Let the input function be \\(x(t) = A \sin(\omega t)\\). Then our ODE is solved by:
$$y(t) = \frac{-A r \omega \cos(\omega t) + A (k - m \omega^2) \sin(\omega t)}{r^2 \omega^2 + (k - m \omega^2)^2}$$
It has an amplitude of
$$\frac{A}{\sqrt{r^2 \omega^2 + (k - m \omega^2)^2}}$$

For completeness, the first derivative is
$$\dot{y}(t) = \frac{A \omega (k - m \omega^2) \cos(\omega t) + A r \omega^2 \sin(\omega t)}{r^2 \omega^2 + (k - m \omega^2)^2}$$
and its amplitude
$$\frac{A \omega}{\sqrt{r^2 \omega^2 + (k - m \omega^2)^2}}$$

The output of our filter is the force exerted by the spring, or \\(k y(t)\\). I also care about the maximum displacement, the maximum velocity, and the maximum force exerted by the damping system.


## Idea 2: Custom MIDI guitar pickup

Conventional guitar pickups produce an analog signal, that is amplified and fed directly into a loudspeaker. Electronic keyboards, on the other hand, can communicate in a digital format called MIDI that encodes (among other things) when notes are played and released. A fun but not so commonly used bridge between these two worlds is the MIDI pickup. These pickups record analog signals just like conventional pickups, but then sample and process the signal so that corresponding MIDI messages can be generated. This enables, for example, a bass guitar to control a synthesizer.

There are two dominant commercial MIDI guitar pickups: the [Fishman TriplePlay](https://www.fishman.com/products/series/tripleplay/) and the [Roland GK-3](https://www.roland.com/global/products/gk-3/). Both cost several hundred dollars. I bet it's possible to make one for far less. I'll try to make one for my five string bass.


### General Design Requirements

Frequency extraction isn't a trivial problem, since even a single note played on a guitar has overtones at many different frequencies. So there's always an element of inference: given an observed frequency spectrum, the problem is to determine which note or notes are most likely to generate it. This problem is especially difficult for some commonly played patterns, like playing two notes an octave apart, in which the frequency spectra of the two notes are very similar. Luckily the problem can be simplified a great deal by processing each string independently. Each string can only sound one note at a time, so we don't have to consider note combinations. All popular commercial MIDI pickups function in this manner.

So the essential components of the system are:

 - a divided pickup that can listen to each string individually
 - a collection of ADC channels that sample the analog signal from each string
 - signal processing software that determines which note is being played on each string (if any)
 - an output system that emits the appropriate MIDI messages


### The Divided Pickup

A pickup is at heart a variable reluctance sensor. It consists of a coil of wire in a magnetic field. When a ferromagnetic object like a steel string vibrates nearby, the field is disturbed and an alternating current is induced in the coil.

Most pickups fall into one of two classes: single coil or humbucking. A single coil pickup works just as already described. A common problem in the modern environment is that in addition to picking up the signal generated by the vibrating guitar strings, you also get a signal from the 60Hz alternating current in nearby power wires. Humbuckers reduce this problem in a clever way: while the signal induced by the strings depends on the orientation of the magnetic field as well as the winding direction of the coil, the signal induced by AC mains only depends on the winding direction. So a humbucker chains two single coil pickups together, where the second has its magnetic field and winding direction reversed. As a result the signal from the strings is picked up twice (the signals from each coil constructively interfere), while the signal from AC power is cancelled out (the signals from each coil destructively interfere). Having a cleaner signal to analyze is a good thing, so I'll make a humbucker. After all, an intermittent 60Hz signal could look a lot like a series of (somewhat sharp) B notes.

There are two main arrangements of coils and magnets in guitar pickups: either the coil is wrapped around a permanent magnet directly, or it is wrapped around a metal slug with high permeability and a magnet is placed underneath. I don't want to have to drill any new holes in my bass, so my pickup will need to be thin. So the former arrangement is probably more appropriate.

The classic DIY coil winding technique involves a hand drill, but people have tried all sorts of other solutions [as well](http://www.tdpri.com/threads/diy-winding-with-a-drill-press.159104/). The CBA does have a specialized coil winding machine, however, so I can indulge in a bit of winding luxury. As an aside though, hand-wound and machine-wound pickups do have a discernable difference in sound. Machines are much steadier than hands, so the individual windings are packed closer together and stay more parallel. This allows more windings to occupy the same space, leading to a louder output, but also increases stray capacitance, which can reduce frequency response in the upper portions of the human hearing range. Hand-wound pickups, with their comparatively messier coils, are ever so slightly quieter but sometimes ever so slightly brighter.

I took the relevant measurements on my bass with a pair of calipers and dove into Fusion. At first I thought I'd 3d print some bobbins and wrap the coils on them, as seen below. Magents could be fit inside, and all ten wound bobbins could be encapsulated in epoxy.

![](/img/01_pickup_v1.jpg#c)

Then Sam encouraged me to try freestanding coils. So instead of using bobbins that stay permanently wrapped with copper wire, I'd wind the coils on a temporary shaft, using temporary guides. The wrapped coils would be glued to themselves, removed from winding shaft, and transfered to a cylindrical magnet (with the same diameter as the winding shaft). I searched online and found that the most commonly used type of magnet is Alnico. But the ones I found [for sale](https://www.stewmac.com/Pickups_and_Electronics/Pickup_Kits_and_Parts/Pickup_Kits_and_Parts_for_Strat/Alnico_5_Polepiece_Magnets.html) were longer than I wanted. So I bought some 3/16" diameter, 1/4" long cylindrical neodymium magnets from [K&J Magnetics](https://www.kjmagnetics.com/). From [McMaster](https://www.mcmaster.com/rotary-shafts) I ordered a four inch long, 3/16" diameter rod to use for winding.

I printed two shaft collars, that can be fixed in place thanks to some press-fit M3 hole inserts and corresponding screws. With this setup I'm ready to wind some coils. However at this stage I decided to refocus my efforts (discussed more below).

![](/img/01_coil_winding_setup.jpg#c)


### Microcontroller Selection

The big question to answer here is whether the system should be monolithic or distributed. In the monolithic design, I try to find one powerful microcontroller than can process all the signals at once. In the distributed design, I process each string on a dedicated microcontroller. The former requires more careful selection of components, but requires less soldering and programming, and is likely more power efficient in the end. It also circumvents the need for communication between microcontrollers. If I'm going to run all the signal chains on one microcontroller, I'll probably want it to support floating point operations in hardware, as I'll probably want to run a lot of DFTs or other common signal processing algorithms.

I also need to decide if I want to use the ADC on the microcontroller, or if I should use an offboard ADC. I have no experience with this, but my guess is that an audio-specific off-chip ADC would sound nicer (i.e. more linear response and lower noise), but that the onboard ones are still fine for the purpose of extracting a pitch --- as long as the sample rate and bit depth are acceptable. For my first take I think it's wise to cut out all components that aren't completely necessary, so I'll try to find a microcontroller with a capable ADC built in.

So overall the relevant microcontroller specs are clock speed, hardware floating point capability, and ADC performance. I set out to perform a somewhat systematic review of the options, but found it to be a Sisyphean task. This is both because there's so many microcontrollers out there, and because it can require a long time with a datasheet to determine just how well the ADC will perform. The latter involves understanding how the ADC is clocked, how the sample rate affects the resolution, how different ADC channels interact, etc.

So eventually I went with the first microcontroller I found that seemed to have decent stats in all the aforementioned departments: the Atmel SAMD51. It has an ARM Cortex M4F processor with a floating point unit, and can sample up to 1 million samples per second at 12-bit resolution, or trade off sampling frequency for higher resolution (up to 16 bits supported in hardware). The problem is that I have never worked with ARM chips before, and they are a lot more complex than their AVR counterparts. To try to overcome this I spent a lot of quality time with the datasheet, and experimented with two different pre-existing SAMD51 boards: an [Adafruit Feather M4](https://www.adafruit.com/product/3857?gclid=Cj0KCQiAr93gBRDSARIsADvHiOrHAq68TLa7fTvkSxp-SecTAZ-jKGF9rtpwsRLK9wJn-houwsa7XHgaAgSjEALw_wcB) and a precursor to Jake's [atkbreadboardboard](https://gitlab.cba.mit.edu/jakeread/atkbreadboardboard), the [mkbreadboardboard](https://gitlab.cba.mit.edu/jakeread/mkbreadboardboard).

![](/img/01_samd51_boards.jpg#c)

Ultimately I decided that progress was too slow on the SAMD51. I'm learning plenty by programming AVRs and don't feel compelled to rush to an ARM. So I decided to switch to an Atmel XMEGA. It doesn't have a floating point unit, and it doesn't have hardware support for 16-bit sampling, but it can still sample millions of times a second. Plus I had just gotten acquianted with its ADC for [machine week](../11_machine_week).


### Algorithms

My go-to algorithm for most audio processing tasks is the [DFT](https://en.wikipedia.org/wiki/Discrete_Fourier_transform). I've used these in the past for beat extraction and it worked pretty well. For this application, however, they aren't quite appropriate. Human perception of pitch is logarithmic, since each octave actually equates to a doubling of pitch. (As a result the difference between the fundamental frequencies of the lowest two notes on my bass is only 1.8Hz, while the difference between the highest two notes is around 20Hz.) But the DFT gives frequency buckets that are linearly distributed in frequency space. Luckily there's a logarithmically spaced counterpart, the [constant-Q transform](https://en.wikipedia.org/wiki/Constant-Q_transform).

So my plan is to perform a constant-Q transform, then perform a least squares optimization to determine which note is most likely to generate the observed overtone distribution (assuming each note generates a harmonic series with roughly exponential decay). Ideally I'd be able to incorporate past states into the model, so I'd really want to compute the probability that a given note is playing conditioned on the observed frequency distribution and prior states.


### Audio & MIDI I/O

The main questions for audio are how to provide gain, and how to orchestrate regular sampling. The XMEGA 128A4U has an internal gain stage, but I'd like to use my own analog circuitry instead so that I can vary the gain continuously. I developed the necessary circuit during [inputs week](../09_input_devices). During that same week I sampled audio in the laziest way: poll the result register, and manually start another conversion when it's finished. This won't work when we're doing a lot of additional computation. So I dove into the datasheet and set up a timer to provide regular interrupts. [This code](https://gitlab.cba.mit.edu/erik/xmega_audio/blob/master/xmega_test/main.c) demonstrates this system, by blinking an LED.

I experimented with MIDI output during [outputs week](../10_output_devices). MIDI is a form of serial communication, so at the time I used an existing board and Neil's bit-banging serial implementation. On the XMEGA I can use the USART peripheral. Alternatively, I can send MIDI over USB, as I did during [wildcard week](../14_wildcard). However by that time I had already come to the following conclusion...


### Reality Check

There are a few weeks left, and though I've made good progress, I'm doubtful I can complete all that I set out to do. I have an analog gain stage, and a sampling system using the XMEGA's timer, interrupts, and ADC, but still need to make the pickup and design the board. Since a lot of this material is new to me, I imagine it will take me a few revisions to get it right. So I definitely don't want to aim for processing each string separately at this point -- that's probably too many spirals away. So, one obvious option is to process all the strings together, and accept that it will only work if one note is played at a time. But there's another idea...


## Idea 3: DIY USB Audio Recording Interface

Instead of turning the audio into MIDI, I will aim to transmit it to my computer via USB. At this point I think this idea has a number of advantages over the MIDI pickup. First, if I write the pitch extraction algorithm now and it doesn't work well, it will be hard to determine if it's the algorithm's fault, or if something is off with my audio sampling. So being able to stream samples to the computer is a helpful first step even for the MIDI project. Second, though learning about pitch extraction algorithms is very interesting to me, it's not the sort of thing I'm in this class to learn --- it's mostly a dive into signal processing math, rather than hardware. Shifting my software focus from constant-Q transforms to USB keeps me tightly coupled to hardware.

I'll stick with the XMEGA because I'm becoming familiar with its registers and don't want to start from scratch with another microcontroller. All the board design, fabrication, and software for this project are described on my [networking](../13_networking) and [wildcard](../14_wildcard) week pages. So here I'll talk about the system integration.

I designed an enclosure in Fusion. Its main chassis is printed, and it has a transparent acrylic top held on by magnets (left over from my pickup plans). The magnets are seated in recesses and held in place with epoxy. It has a 1/4" audio jack input, a knob to control the pre-amp gain, and a MIDI output. Currently I'm not using the MIDI output, but I want to leave the option open for the future. These three features are mounted to the enclosure, so that they can't put any mechanical strain on the circuit board. The MIDI connector and circuit board are held in place using plastic thread-forming screws. The fusion file is [here](/designs/01_enclosure.f3z).

![](/img/01_enclosure.jpg#c)

Here's the result.

![](/img/01_end_product.jpg#c)

