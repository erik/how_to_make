+++
title = "CNC Cutting"
date = "2018-09-13"
menu = "main"
weight = 3
+++


## A Laser Cut Construction Kit

Files: [pentagon.dxf](/designs/02_pentagon.dxf), [connector.dxf](/designs/02_connector.dxf)

One of the assignments this week is to make a parametric cardboard construction kit on a laser cutter. I like platonic solids, so I'm going to base my kit on a dodecahedron. I sketched these tiles and connectors in Fusion 360.

![](/img/02_design.jpg#c)

For fit, there are three main parameters: thickness, kerf, and epsilon. Thickness and kerf are used to make shapes that should exactly interlock, and epsilon determines how much I intentionally undersize certain components in order to compress the cardboard. During our group laser characterization, we found the cardboard to be 0.17", the kerf to be about 0.1", and that it was beneficial to compress the cardboard in joinery by another 0.1". The other parameters control geometry.

![](/img/02_params.jpg#c)

Cutting proceeded smoothly. Some of the connectors (and one of the tiles) ended up with critical geometry between corrugations, making them unusably weak.

![](/img/02_dodecasoup.jpg)

Here are two constructions.

![](/img/02_penta_stand.jpg)
![](/img/02_dodecahedron.jpg#c)

The extra degree of freedom (from using slots instead of tabs) allows more than two tiles to meet at an edge.

![](/img/02_joint_detail.jpg#c)


## A Practical Analysis of Vinyl Cutting Machines

Files: [shannon.svg](/designs/02_shannon.svg)

This past week we learned how to use a vinyl cutter. This is a new tool for me and I'm excited to try it out. As a small token of appreciation for formalizing digital circuit design, I decided to cut a portrait of Claude Shannon to apply to my desktop computer.

Not surprisingly, there aren't many vector images of Claude Shannon online. So I edited a raster image in Photoshop until I had a nice looking two tone version. I also managed to vectorize this image in Photoshop, but whenever I tried to save the results the program got stuck. So I installed Inkscape and used it for bitmap tracing.

![](/img/02_source_collage.jpg#c)

Despite all the detail, the vinyl cutter only took a few minutes to cut my design. Weeding, however, took two hours. Finally I removed a panel from my desktop's enclosure and transferred the image.

![](/img/02_weeding_collage.jpg#c)

I'm quite pleased with the result! The vinyl cutter is a tool I now see myself using regularly.

![](/img/02_shannon_desktop.jpg#c)

