+++
title = "Electronics Design"
date = "2018-10-09"
menu = "main"
weight = 7
+++

## PCB Design

Files: [echo.sch](/designs/05_echo.sch) [echo.brd](/designs/06_echo.brd) [echo_top.png](/designs/06_echo_top.png) [echo_cutout.png](/designs/06_echo_cutout.png) (both 2000 dpi)

This week we're learning how to go all the way from pcb design through milling, stuffing, and programming. I should end up with a board that can talk to a computer over a serial connection (masquerading as a USB device).

### Design

Since I've been using Fusion 360 so much, I decided to give EAGLE a try. (I'm also curious to use KiCad, but at this point I want to get comfortable in one environment before trying another.) After importing the fab library and design rules, I was ready to go.

Drawing the schematic was simple enough. From the [reference design](http://academy.cba.mit.edu/classes/embedded_programming/index.html#echo) I just added a switch and an LED (with current limiting resistor). I checked that all GPIO pins on the ATTINY44 have internal pullup resistors, so I'm going to rely on those to pull my input high when the switch is open. To get a value for the current limiting resistor, I plugged in the current rating and forward voltage drop of our LEDs to an [online calculator](https://www.digikey.com/en/resources/conversion-calculators/conversion-calculator-led-series-resistor). I found that 330 ohms would be ideal, so I went with the closest value we stock in bulk: 499 ohms.

![](/img/05_schematic.jpg#c)

Drawing the traces was more challenging. It took me a few tries before I was able to pack the components in a reasonable amount of space with relatively short routes. I'm still not super pleased with how indirect the ground path to the microcontroller is. I look forward to designing more boards and getting a better intuition for circuit layout.

![](/img/05_board.png#c)

### Production

When I started milling my design, it was obviously too large. Turns out the DPI of images exported by EAGLE on retina MacBooks ends up twice as high as claimed. Luckily this is easily fixed in mods by overriding the value embedded in the png.

At this point I feel comfortable stuffing boards. It went by pretty quickly and I'm happy with all the joints.

### Programming

It had been a few years since I've flashed a microcontroller so I was uncertain how many issues I should expect to run into. Luckily things went pretty smoothly. I encountered a few "device not found" errors that were circumvented by unplugging and replugging the programmer and boards. I just used Neil's [echo code](http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.echo.c).

![](/img/05_pcb_plus_ftdi.jpg#c)

Communication with my computer was achieved!

![](/img/05_let_there_be_bits.png#c)

