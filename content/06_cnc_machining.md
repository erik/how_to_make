+++
title = "CNC Machining"
date = "2018-10-17"
menu = "main"
weight = 8
+++


## Make Something Big
Files: [code](https://gitlab.cba.mit.edu/erik/htmaa_shelves) (generates SVGs and ShopBot files)

I've been waiting for this week. Having just moved from California, my apartment is in need of some more furniture. My most pressing need is additional shelving.


### Design

I want a shelving unit that fits exactly between two doors in my bedroom, and occupies all the space between the floor and ceiling. To enable laziness, I'm also taking as a design constraint the ability to assemble and disassemble the shelves without any glue or fasteners. I'm aiming for solid slot joints.

I started by drawing a parametric model in Fusion 360.

![](/img/06_fusion_design.jpg#c)

I then wrote my own code to generate the 2d profiles of each piece, rather than exporting them from Fusion. This is mostly because I'm curious to think about what primitives a code based CAD environment for panel structures should provide. But it also means I don't have to fix a few nuisances in the Fusion model, such as some infinitesimally thin surfaces left over from extruded cuts, and the fact that some beams repeatedly move to new locations despite the fact that they're in a rigid group (the only fix I've found is deleting the pattern feature and recreating it, with identical settings).

My code exports SVG images, because they're easy to write. (On this site it's in a raster format, since my SVGs don't respect the viewport defaults that browsers use.)

![](/img/06_2d_design.png#c)


### Test Model

To ensure that my generated 2d patterns are correct, I cut a 1/4 scale model out of cardboard with a laser cutter. It fits together nicely. This is a great way to ensure that the model doesn't have any glaring mistakes. Though in this case, I actually found a few minor issues later that were too small to detect at this scale.

![](/img/06_scale_model.jpg#c)


### Machining: First Try

I reserved a block of time on Tuesday. The first hurdle I encountered is that VCarve can't import SVG files. So to load my models, I had to open them in Inkscape, and convert them to EPS. (Rhino won't load SVGs either.)

The second issue I encountered was that VCarve Pro is very hesitant to navigate into narrow slots. I'm using a quarter inch router bit (more on this later), and I would like to use it to cut some quarter inch slots. But VCarve wouldn't insert toolpaths until I widened the slots to 0.255 inches. This difference isn't visible, but it's certainly enough to make otherwise tight slot joints sloppy and loose. At the time I figured I could live with this, and expanded all of my model's quarter inch slots.

The quarter inch endmill should have no problem machining half inch slots, and I intentionally undersized these to get a tight fit. Unfortunately, after cutting my test pieces I found that the fit was still very loose. Calipers confirmed that the slots were quite a bit wider than in the model. And while investigating this issue, I also noticed that my whole shelving unit was about a quarter inch wider than it should be. The error in the overall length of my model turned out to be a bug in my SVG generation.

I investigated the slot size issue in more detail later, and found that it was mostly due to tool deflection. Cutting climb vs conventional causes a very noticeable difference in part size (at least as far as joint fit is concerned). This surprised me since the deflection here was greater here than with the mills I used for metal at [Plethora](https://www.plethora.com/). But of course there we used Haas machines with giant cast iron frames. Apparently the ShopBot is floppier than I thought.


### Generating GCode

Overall, I was getting pretty frustrated with VCarve. So I decided I might as well generate my own gcode (ok, in this case ShopBot code, not gcode). By using line segments and arcs, I can make the ShopBot files a lot smaller than VCarve (though I'm not sure if it's VCarve that exploded the arcs, or Inkscape). I'll also know exactly what commands the machine is getting, so I don't have to worry that my part size issues are from file conversion issues or VCarve bugs.

This required me to implement path offsetting for line segments and arcs, ShopBot code generation, and my own system for tabs. Check out my code if you want to see the gory details.


### Machining: Second Try

On Wednesday I returned to the shop. When I first started trying to machine, I was dismayed that the ShopBot wouldn't ask to start the spindle. I based my ShopBot code generator on the [ShopBot Command Reference](http://www.shopbottools.com/ShopBotDocs/files/ComRef.pdf), so I'm not sure what's different about our setup. Ultimately I just copied the preamble from some ShopBot files I had exported from VCarve.

After that, it was smooth (if loud) sailing.

<video controls src="/img/06_shopbot.mp4"></video>

First I cut a quick slot joint test piece. Normally this would require re-exporting files from CAD, and doing the CAM again in VCarve. But since my design and gcode generation take place in one software pipeline, I just had to rerun my code to get all my updated ShopBot files.

![](/img/06_slot_test.jpg#c)

After that it was just a matter of waiting for the machine, and feeding it stock. I had four panels in total (oops, looks like I forgot to take a picture of one).

![](/img/06_panel_1.jpg#c)
![](/img/06_panel_2.jpg#c)
![](/img/06_panel_3.jpg#c)

Here's the start (and end) point of one of the longer pieces. The effects of hysteresis and tool deflection are clearly visible (yes, the lines are programmed straight).

![](/img/06_hysteresis.jpg#c)

### Assembly

Remember when I mentioned that cutting climb vs conventional had a noticeable impact on part size? I hadn't figured out how important that was just yet, which caused me some problems. I cut everything climb. Which for the most part meant that all my parts were oversized by the same amount. But in a few places, the size changes interfered constructively. For example, the shelves have tabs that fit into holes in the main vertical supports. Tool deflection meant that the shelf tabs were a little wider than modeled, and that the cutouts on the supports were a little less wide than modeled. So I had to do some sanding to get them to fit. This was a lot of work, but they did fit perfectly in the end.

I'm really happy with the end result. Here's my room, before and after. I can't imagine going back.

![](/img/06_before.jpg#c)
![](/img/06_after.jpg#c)

