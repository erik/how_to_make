+++
title = "3d Printing and Scanning"
date = "2018-10-02"
menu = "main"
weight = 6
+++

## Printing

Files: [starflake.io](/designs/04_starflake.io) (render with [libfive Studio](https://libfive.com/studio/))

This week's primary assignment is relatively unconstrained: design and print something that couldn't be made with subtractive techniques. I decided to explore curved surfaces with severely constrained accessibility. This was also a good excuse to learn [libfive](https://libfive.com/), since functional shape representations enable a more diverse palette of interesting deformations than traditional b-reps.

### Design

I modeled a small stellated dodecahedron, where each of the stellations is twisted along its axis. In the end, the model I printed looks like this.

![](/img/04_starflake.jpg#c)

Designing it was mostly a matter of working out trigonometric relations to ensure the various pieces
would correctly mate. There were many times I made errors in my calculations, and built shapes with
large gaps or unappealing self-intersections. A number of these steps would have been more easily
and intuitively expressed as constraints between various sketch and body entities. (It could be an
interesting research direction to try to merge the functional and constraint based CAD worlds.) But
ultimately modeling such a shape in traditional CAD would have been much more difficult. The
twisting operation in particular, while a few lines of code in libfive, would be nearly impossible
to define in a traditional CAD environment if not provided as a primitive operation.

Using libfive also means that my model is inherently parametric. To give it the best shot at being
printed without errors, I chose to make it quite chunky. It's also interesting with much thinner
beams.

![](/img/04_thin_starflake.jpg#c)

### Fabrication

My design was brought to life by CBA's Stratasys Eden printer, using a translucent plastic.

![](/img/04_print_1.jpg#c)

(The indentations on the ends of some of the spires are my fault, not the printer's. The part lasted about 10 minutes before I dropped it.)

The way this material transmits and diffuses light is quite nice. With the right lighting, it almost looks like ice.

![](/img/04_print_2.jpg#c)

The layers aren't noticeable from afar, but they are quite apparent up close.

![](/img/04_print_3.jpg#c)


## Scanning

For the same reasons it couldn't be manufactured subtractively, the above shape is a very poor
candidate for 3d scanning. Which is exactly why I wanted to try it.

Results were... as expected.

![](/img/04_scan.jpg#c)

