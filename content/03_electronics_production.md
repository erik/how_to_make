+++
title = "Electronics Production"
date = "2018-09-22"
menu = "main"
weight = 4
+++

## Making a Microcontroller Programmer

Files: [Brian's design](http://fab.cba.mit.edu/classes/863.16/doc/projects/ftsmin/index.html)

This week's topic is how to make printed circuit boards (PCBs). Our assignment is two-fold: use a desktop format mill to cut the board, then solder on the components. The board will be used later this semester to program other microcontrollers.

### Milling

We're using a Roland Modela MDX-20 milling machine, controlled with [mods](http://mods.cba.mit.edu/). The mods interface takes black and white PNG images as input, calculates toolpaths, and sends the resulting G-code to the milling machine.

![](/img/03_mods.jpg#c)

In groups we milled test patterns to characterize the design rules for our PCB milling capabilities. As expected, with a 1/64" end mill traces must be at least 1/64" apart. Traces thinner than a few thousandths of an inch lack structural integrity and are easily knocked off.

![](/img/03_line_test_pattern.png#c)
![](/img/03_line_test_result.jpg#c)

The height of the end mill as loaded is Z home, so it's important to make sure it's flush with the surface to be cut. The first time we loaded the 1/64" tool, the act of tightening the set screws pulled it up a few thousandths of an inch. It still looked flush with the material, but as can be seen below it did not cut the traces properly. Later we realized we could slide a sheet of paper between the end mill and the PCB.

![](/img/03_failed_test.jpg#c)

After the test pattern, I milled a programmer.

![](/img/03_milling.jpg#c)

### Stuffing

I had never tried to solder surface mount components before, so I wasn't sure how it would go. My first attempt was a disappointment: crystallized joints across the board (ha).

![](/img/03_lead_free.jpg#c)

A few hours later I realized that I had used lead-free solder, so I decided I'd stuff another board using good old fashioned leaded solder. It was far easier, and the results were much more satisfying. Now I know firsthand why lead is still a common ingredient in solder.

![](/img/03_glorious_lead.jpg#c)

Here are both boards next to each other; lead-free on top, leaded below.

![](/img/03_two_boards.jpg#c)

Looking forward to programming these programmers!

### Thickening

Our PCBs are a bit thinner than the spec for USB connectors. So I cut out the profile on the vinyl cutter and stuck 8 layers on the bottom of my boards. This makes them stick in USB ports much more firmly.

![](/img/03_vinyl_bottom.jpg#c)

