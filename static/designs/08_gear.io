(set-bounds! [-10 -10 -10] [10 10 10])
(set-quality! 11)
(set-resolution! 25)

(define-shape (gear x y z)
  (max (min (- 7.1 (sqrt (+ (square x) (square y)))) (- (modulo (+ 3.14159 (atan y x)) 0.523599)
  (- (sqrt (- (expt (/ (max 5.63816 (sqrt (+ (square x) (square y)))) 5.63816) 2) 1))
  (acos (/ 5.63816 (max 5.63816 (sqrt (+ (square x) (square y))))))))
  (- (- (- (sqrt (- (expt (/ (max 5.63816 (sqrt (+ (square x) (square y)))) 5.63816) 2) 1))
  (acos (/ 5.63816 (max 5.63816 (sqrt (+ (square x) (square y))))))))
  (+ -0.291608 (modulo (+ 3.14159 (atan y x)) 0.523599)))) (- 4.6 (sqrt (+ (square x) (square y)))) (- -3 z) (- z 3))
)


(difference
(cylinder-z 10 3)
(taper-xy-z gear (vec3 0 0 0) 6 0)
(cylinder-z 1.5 8)
)

