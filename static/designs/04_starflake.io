(set-bounds! [-10 -10 -5] [10 10 15])
(set-quality! 9)
(set-resolution! 10)


;;;;;;;;;;;;;;; Dodecahedron Constants ;;;;;;;;;;;;;;;
(define dihedral (- pi (atan 2)))
(define half_dihedral (/ dihedral 2))


;;;;;;;;;;;;;;; Model Parameters ;;;;;;;;;;;;;;;
; distance from the center of a pentagonal face to any of its outer vertices
(define r_face_outer_vertex 3)
; distance from the center of a pentagonal face to the midpoint of any of its (outer) edges
(define r_face_outer_edge (* r_face_outer_vertex (cos (/ pi 5))))
; cross-sectional thickness of beams (for either dimension)
; (actually I'm not sure my extrusions and differences respect this... need to check my math)
(define thickness 2.25)
; distance from the center of a pentagonal face to any of its inner vertices
(define r_face_inner_vertex (- r_face_outer_vertex (/ thickness (tan half_dihedral))))
; distance from the center of a pentagonal face to the midpoint of any of its (inner) edges
(define r_face_inner_edge (* r_face_inner_vertex (cos (/ pi 5))))
; distance from the center of the dodecahedron to the center of any inner pentagonal face
(define r_body_inner_face (* r_face_outer_edge (tan half_dihedral)))


;;;;;;;;;;;;;;; Dodecahedron Translation Utilities ;;;;;;;;;;;;;;;
; Translates a pentagonal tile at the origin to become one of the lower row pentagons of a dodecahedron
(define (repos1 shape)
    (rotate-z
        (move
            (rotate-x
                (move
                    shape
                    (vec3 0 (- r_face_outer_edge) 0)
                )
                (- dihedral pi)
            )
            (vec3 0 (- r_face_outer_edge) 0)
        )
        (/ pi 5)
    )
)

; Translates a pentagonal tile at the origin to become the top tile of a dodecahedron
(define (repos2 shape)
    (move
        (rotate-x shape pi)
        (vec3 0 0 (* 2 r_body_inner_face))
    )
)

; Translates a pentagonal at the origin to become one of the upper row pentagons of a dodecahedron
(define (repos3 shape)
    (rotate-z
        (move
            (rotate-x
                (move
                    shape
                    (vec3 0 (- r_face_outer_edge) 0)
                )
                dihedral
            )
            (vec3 0 r_face_outer_edge (* 2 r_body_inner_face))
        )
        (/ pi 5)
    )
)

; Duplicates one of the pentagonal tiles in the upper or lower rows of a dodecahedron to complete the row
(define (tile5 shape)
    (union
        shape
        (rotate-z shape (/ (* 2 pi) 5))
        (rotate-z shape (/ (* 4 pi) 5))
        (rotate-z shape (/ (* 6 pi) 5))
        (rotate-z shape (/ (* 8 pi) 5))
    )
)


;;;;;;;;;;;;;;; Deformation Utilities ;;;;;;;;;;;;;;;
(define (twist-z shape scale)
    (remap-shape (shape x y z)
        (let
            ((z2 (* scale z)))
            (+ (* (cos z2) x) (* (sin z2) y))
        )
        (let
            ((z2 (* scale z)))
            (- (* (cos z2) y) (* (sin z2) x))
        )
        z
    )
)


;;;;;;;;;;;;;;; Dodecahedral Frame Shapes ;;;;;;;;;;;;;;;
; A pentagonal section that can be duplicated to form a dodecahedron
(define-shape (dodeca_frame x y z)
    (difference
        (taper-xy-z
            (extrude (polygon r_face_outer_vertex 5) 0 thickness)
            (vec3 0 0 0)
            r_body_inner_face
            0)
        (extrude (polygon r_face_inner_vertex 5) -0.1 thickness)
    )
)


;;;;;;;;;;;;;;; Dodecahedral Stellation Shapes ;;;;;;;;;;;;;;;
; One hollow stellation for a small stellated dodecahedron
(define-shape (stellation x y z)
    (let
        (
            (h1 (* 2 r_face_outer_edge))
            (h2 (* 2 r_face_inner_edge))
        )
        (difference
            (taper-xy-z
                (extrude (polygon r_face_outer_vertex 5) (- h1) 0)
                (vec3 0 0 0)
                (- h1)
                0
            )
            (taper-xy-z
                (extrude (polygon r_face_inner_vertex 5) (- h2) 0.1)
                (vec3 0 0 0)
                (- h2)
                0
            )
        )
    )
)

; A single triangular prism that overlaps with a wall of a stellation
(define-shape (stellation_tool x y z)
    (rotate-z
        (move
            (rotate-x
                (let*
                    (
                        (x (* r_face_outer_vertex (sin (/ pi 5))))
                        (len (* x (tan (/ (* 2 pi) 5))))
                        (height (/ r_face_outer_edge (cos (- (/ pi 2) dihedral))))
                    )
                    (taper-xy-z
                        (extrude
                            (scale-xyz
                                (triangle
                                    (vec2 x 0)
                                    (vec2 (- x) 0)
                                    (vec2 0 len)
                                )
                                ; TODO make these meaningful
                                (vec3 0.5 0.5 1)
                            )
                            0
                            thickness
                        )
                        (vec3 0 0 0)
                        height
                        0
                    )
                )
                (- dihedral pi)
            )
            (vec3 0 (- r_face_outer_edge) 0)
        )
        (/ pi 5)
    )
)

; A frame of a stellation
(define-shape (stellation_frame x y z)
    ; TODO why the let?
    (let
        (
            (h1 (* 2 r_face_outer_edge))
            (h2 (* 2 r_face_inner_edge))
        )
        (difference
            stellation
            (tile5 stellation_tool)
        )
    )
)

; One hollow stellation for a small stellated dodecahedron
(define-shape (twisted_stellation_frame x y z)
    (twist-z
        stellation_frame
        0.3
    )
)


;;;;;;;;;;;;;;; Everlasting Gobstopper ;;;;;;;;;;;;;;;
(define-shape (everlasting_gobstopper x y z)
    (union
        dodeca_frame
        (tile5 (repos1 dodeca_frame))
        (repos2 dodeca_frame)
        (tile5 (repos3 dodeca_frame))
        twisted_stellation_frame
        (tile5 (repos1 twisted_stellation_frame))
        (repos2 twisted_stellation_frame)
        (tile5 (repos3 twisted_stellation_frame))
    )
)


;;;;;;;;;;;;;;; main ;;;;;;;;;;;;;;;
everlasting_gobstopper

